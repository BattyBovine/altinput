# README #

altinput is a launcher for Tetris The Grandmaster 3: Terror Instinct created by AltimorTASDK on Github. It includes an alternate input method that reduces input lag from the original arcade release, which can be configured with the included configuration file.

### How do I get set up? ###

Simply open the .sln file in Visual Studio 2017 or higher and compile. Drop all three compiled binaries into the folder for Tetris The Grandmaster 3: Terror Instinct, and use tgm3_launcher.exe to start the game.

### How is this different from the original version? ###

Mainly, the keyboard input code has been updated to allow two players to share one keyboard, which could be useful for more easily building a custom arcade cabinet for the game, as only one input device will be necessary for both players, and a keyboard is recognisable on any system with no complex configuration steps. Once I figure out how to make it work, I also want to add buttons for the Test, Service, and Coin inputs, so that it can function in situations where Free Play isn't desirable.