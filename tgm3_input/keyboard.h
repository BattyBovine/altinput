#pragma once

#include "base_input.h"
#include <list>
#include <map>

class keyboard : public base_input {
	// TGM3 buttons
	uint16_t buttons[2];
	uint16_t operator_buttons;

	// Directional buttons from the previous update
	unsigned short old_dir_buttons[2];

	// List containing which direction keys are pressed
	std::list<unsigned short> direction_keys[2];

	// Virtual key -> Button mask table
	uint16_t key_mask_map[2][0xFF];
	uint16_t coin_key_mask_map[2][0xFF];
	uint16_t operator_key_mask_map[0xFF];

public:
	// Raw input usage
	std::vector<int> get_usage() override
	{
		return { { 6 } };
	}

	// Load keycodes from config
	void init(const config &cfg) override;

	// Update direction_keys and buttons
	void update(const tagRAWINPUT *input) override;

	// Clear direction keys list too
	void clear_buttons() override
	{
		buttons[0] = 0;
		buttons[1] = 0;
		direction_keys[0].clear();
		direction_keys[1].clear();
	}

	// buttons accessors
	uint16_t get_buttons_1p() const override
	{
		return buttons[0] & 0xFFFF;
	}

	uint16_t get_buttons_2p() const override
	{
		return buttons[1] & 0xFFFF;
	}

	uint16_t get_buttons_operator() const override
	{
		return operator_buttons & 0xFFFF;
	}
};