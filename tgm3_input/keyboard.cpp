#define WIN32_LEAN_AND_MEAN
#include "keyboard.h"
#include "../config.h"
#include <Windows.h>

/**
 * init - Initialize an input device from a config
 * @cfg:	Config object
 *
 * Load all the keycodes from the config
 */
void keyboard::init(const config &cfg)
{
	const int up[2]			= { cfg.value_int('W', "keyboard.player1.up"), cfg.value_int('T', "keyboard.player2.up") };
	const int down[2]		= { cfg.value_int('S', "keyboard.player1.down"), cfg.value_int('V', "keyboard.player2.down") };
	const int left[2]		= { cfg.value_int('A', "keyboard.player1.left"), cfg.value_int('F', "keyboard.player2.left") };
	const int right[2]		= { cfg.value_int('D', "keyboard.player1.right"), cfg.value_int('G', "keyboard.player2.right") };

	const int A[2]			= { cfg.value_int('H', "keyboard.player1.A"), cfg.value_int('U', "keyboard.player2.A") };
	const int B[2]			= { cfg.value_int('J', "keyboard.player1.B"), cfg.value_int('I', "keyboard.player2.B") };
	const int C[2]			= { cfg.value_int('K', "keyboard.player1.C"), cfg.value_int('O', "keyboard.player2.C") };
	const int D[2]			= { cfg.value_int('L', "keyboard.player1.D"), cfg.value_int('P', "keyboard.player2.D") };

	const int start[2]		= { cfg.value_int(VK_RETURN, "keyboard.player1.start"), cfg.value_int(VK_OEM_5, "keyboard.player2.start") };
	const int service[2]	= { cfg.value_int('C', "keyboard.player1.service"), cfg.value_int('B', "keyboard.player2.service") };
	const int coin[2]		= { cfg.value_int('N', "keyboard.player1.coin"), cfg.value_int('M', "keyboard.player2.coin") };
	
	const int test			= cfg.value_int('Z', "keyboard.test");

	for (unsigned short &association : operator_key_mask_map)
		association = 0;

	for (uint8_t player = 0; player < 2; player++)
	{
		for (unsigned short &association : key_mask_map[player])
			association = 0;
		for (unsigned short &association : coin_key_mask_map[player])
			association = 0;

		key_mask_map[player][up[player]] = mask_up;
		key_mask_map[player][down[player]] = mask_down;
		key_mask_map[player][left[player]] = mask_left;
		key_mask_map[player][right[player]] = mask_right;

		key_mask_map[player][A[player]] = mask_A;
		key_mask_map[player][B[player]] = mask_B;
		key_mask_map[player][C[player]] = mask_C;
		key_mask_map[player][D[player]] = mask_D;

		key_mask_map[player][start[player]] = mask_start;

		key_mask_map[player][service[player]] = mask_srv;

		coin_key_mask_map[player][coin[player]] = mask_coin;

		buttons[player] = 0;
	}

	operator_key_mask_map[test] = mask_test;
	operator_buttons = 0;
}

/**
 * update - Update the bits in buttons
 * @input:	Raw input data pointer
 *
 * Make the most recently pressed directional key take priority
 */
void keyboard::update(const tagRAWINPUT *input)
{
	if (input->header.dwType != RIM_TYPEKEYBOARD)
		return;

	const USHORT vkey = input->data.keyboard.VKey % 0xFF;
	const bool down = (input->data.keyboard.Flags & RI_KEY_BREAK) == 0;

	for (uint8_t player = 0; player < 2; player++)
	{
		const uint16_t mask = key_mask_map[player][vkey];
		buttons[player] = down ? (buttons[player] | mask) : (buttons[player] & ~mask);

		const uint16_t dir_buttons = buttons[player] & (mask_up | mask_down | mask_left | mask_right);
		buttons[player] &= ~dir_buttons;

		const short changed = dir_buttons ^ old_dir_buttons[player];
		const short pressed = changed & dir_buttons;
		const short released = changed & old_dir_buttons[player];

		for (uint16_t i = 0; i < std::numeric_limits<decltype(dir_buttons)>::digits; i++)
		{
			const uint16_t mask = 1 << i;
			if (pressed & mask)
				direction_keys[player].push_front(mask);
			else if (released & mask)
				direction_keys[player].remove(mask);
		}

		if (direction_keys[player].size() != 0)
			buttons[player] |= direction_keys[player].front();

		old_dir_buttons[player] = dir_buttons;

		if (!down && coin_key_mask_map[player][vkey])
		{
			coin_inserted_flags[player] = true;
		}
	}

	operator_buttons = down ? (operator_buttons | operator_key_mask_map[vkey]) : (operator_buttons & ~operator_key_mask_map[vkey]);

	return;
}