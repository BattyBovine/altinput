#pragma once

#include <vector>

class config;
struct tagRAWINPUT;

class base_input {
protected:
	// Game button bitmasks
	static constexpr uint16_t mask_up =		0b0000000000100000;
	static constexpr uint16_t mask_down =	0b0000000000010000;
	static constexpr uint16_t mask_left =	0b0000000000001000;
	static constexpr uint16_t mask_right =	0b0000000000000100;

	static constexpr uint16_t mask_A =		0b0000000000000010;
	static constexpr uint16_t mask_B =		0b0000000000000001;
	static constexpr uint16_t mask_C =		0b1000000000000000;
	static constexpr uint16_t mask_D =		0b0100000000000000;

	static constexpr uint16_t mask_start =	0b0000000010000000;
	static constexpr uint16_t mask_srv =	0b0000000001000000;
	static constexpr uint16_t mask_coin =	0b0000000000000001;

	static constexpr uint16_t mask_test =	0b1000000000000000;

	uint16_t coin_inserted_flags[2] = { false, false };
	uint8_t coin_count_delay = 0;

public:
	// Usage for raw input device
	virtual std::vector<int> get_usage() = 0;

	// Called every time a WM_INPUT message is received
	virtual void update(const tagRAWINPUT *input) = 0;

	// Return TGM3 buttons
	virtual uint16_t get_buttons_1p() const = 0;
	virtual uint16_t get_buttons_2p() const = 0;

	virtual uint16_t get_buttons_operator() const = 0;

	virtual bool coin_inserted(uint8_t player)
	{
		coin_count_delay++;

		if (coin_inserted_flags[player] && ((coin_count_delay>>3) & 0x01) == player)
		{
			coin_inserted_flags[player] = false;
			return true;
		}
		return false;
	}

	// Must be used on alt tab
	virtual void clear_buttons() = 0;

	// Initialize from config
	virtual void init(const config &cfg) = 0;
};
